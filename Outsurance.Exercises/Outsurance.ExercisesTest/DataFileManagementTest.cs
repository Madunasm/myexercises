﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.Exercises;

using System.Linq;
using System.Text;

namespace Outsurance.ExercisesTest
{
    [TestClass]
    public class DataFileManagementTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GetDataFromSvcFile_WhenDataFilePathPropertyIsEmpty_ShouldThrowArgumentException()
        {
            // arrange    
            DataTable dtData = new DataTable();
            DataFileManagement dfmDatafile = new DataFileManagement();   
        
            // act           
            dtData = dfmDatafile.GetDataFromCsvFile();

            // assert is handled by ExpectedException
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WriteDataToFile_WhenDataListIsNull_ShouldThrowArgumentNullException()
        {
            // arrange    
            List<string> lData = null; 
            DataFileManagement dfmDatafile = new DataFileManagement();

            // act           
            dfmDatafile.WriteDataToFile(lData);

            // assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WriteDataToFile_WhenDataFilePathPropertyIsEmpty_ShouldThrowArgumentException()
        {
            // arrange 
            List<string> lData = new List<string>();    
            DataFileManagement dfmDatafile = new DataFileManagement();

            // act           
            dfmDatafile.WriteDataToFile(lData);

            // assert is handled by ExpectedException
        }
    }
}
