﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.Exercises;


namespace Outsurance.ExercisesTest
{
    [TestClass]
    public class StreetAddressTest
    {
        [TestMethod]
        public void StreetAddress_WithValid_NumberAndName()
        {
            //arrange
            string strAddress = "130 Main Street";
            StreetAddress saAddress = new StreetAddress(strAddress);           
            //act
            string strNumber = saAddress.HouseNumber;
            string strName = saAddress.StreetName;

            //Assert
            Assert.AreEqual("130", strNumber);
            Assert.AreEqual("Main Street", strName);
        }

        
       
    }
}
