﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace Outsurance.Exercises
{
    /// <summary>
    /// Class to handle data file operations 
    /// </summary>
    public class DataFileManagement
    {
        public string DataFilePath { get; set; }
        public DataFileManagement() { }
        /// <summary>
        /// Reads the input data file
        /// </summary>
        /// <returns>Datatable of data rows and column information</returns>
        public DataTable GetDataFromCsvFile()
        {
             if (string.IsNullOrEmpty(DataFilePath))
                throw new ArgumentException("File path cannot be null or empty", "DataFilePath");


            DataTable csvCustomerData = new DataTable();
            using (TextFieldParser csvReader = new TextFieldParser(DataFilePath))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string[] colFields = csvReader.ReadFields();
                //Add Column to the datatable
                foreach (string column in colFields)
                {
                    DataColumn datacolumn = new DataColumn(column);
                    datacolumn.AllowDBNull = true;
                    csvCustomerData.Columns.Add(datacolumn);
                }

                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    csvCustomerData.Rows.Add(fieldData);
                }
            }
                     
            return csvCustomerData;
        }
        /// <summary>
        /// Write the list of string informatios to the outfile
        /// </summary>
        /// <param name="dataList">a List of string</param>
        public void WriteDataToFile(List<string> dataList)
        {
            if (dataList == null)
                throw new ArgumentNullException("DataList cannot be null", "DataToWriteToFile");

            if (string.IsNullOrEmpty(DataFilePath))
                throw new ArgumentException("File path cannot be null or empty", "DataFilePath");

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(DataFilePath))
            {
                foreach (string line in dataList)
                {
                    file.WriteLine(line);
                }
            }                   
        }
    }
}
