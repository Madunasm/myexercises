﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outsurance.Exercises
{
    /// <summary>
    /// Class of data, with firstname and lastname
    /// </summary>
    public class Customers
    {        
        private List<string> Names { get; set; }       
        public Customers (DataTable customerData)
        {
            Names = new List<string>();
            foreach (DataRow row in customerData.Rows)
            {
                Names.Add(row[0].ToString()); // FirstName
                Names.Add(row[1].ToString()); // LastName
            }
        }
        /// <summary>
        /// Method to sorted name (firstname and lastname), descending by frequency and ascending by name
        /// </summary>
        /// <returns>Ordered Name Frequency list</returns>
        public List<string> GetOrderedNameFrequency()
        {    
            List<string> NameFrequency = new List<string>();
            foreach (var line in Names.GroupBy(info => info)
                         .Select(group => new
                         {
                             Name = group.Key,
                             Count = group.Count()
                         })
                         .OrderBy(y => y.Name).OrderByDescending(x => x.Count))
            {
                NameFrequency.Add(string.Format(line.Name + "," + line.Count));               
            } 
            return NameFrequency;
        }
    }
}
