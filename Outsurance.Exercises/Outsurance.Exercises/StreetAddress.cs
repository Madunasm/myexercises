﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Outsurance.Exercises
{   
    /// <summary>
    /// Class to handle the street address format and return the house number and street name object
    /// </summary>
    public class StreetAddress
        {
            public string HouseNumber { get; set; }            
            public string StreetName { get; set; }
        /// <summary>
        /// Pattern on the street address if house number followed by street name
        /// </summary>
        /// <returns>string regex pattern for the street address</returns>
            private string AddressPattern()
            {
                var pattern = "^" +                                                       // beginning of string
                                "(?<HouseNumber>\\d+)" +                                  // 1 or more digits
                                "(?:\\s+(?<StreetName>.*?))" +                            // whitespace + anything
                                "(?:" +                                                   // group (optional) {                    
                                ")?" +                                                    // }
                                "$";                                                      // end of string

                return pattern;
            }
            public StreetAddress() { }
            public StreetAddress(string address)
            {
                var re = new Regex(AddressPattern());
                if (re.IsMatch(address))
                {
                    var m = re.Match(address);
                    HouseNumber = m.Groups["HouseNumber"].Value;
                    StreetName = m.Groups["StreetName"].Value;
                }
                else
                    StreetName = address;
            }
            
        }
    /// <summary>
    /// Class to hold the list of all street addresses
    /// </summary>
    public class StreetAddresses
    {
        private List<StreetAddress> Addresses { get; set; }       
        public StreetAddresses (DataTable customerData)
        {
            Addresses = new List<StreetAddress>();
            StreetAddress address = new StreetAddress();
            foreach (DataRow row in customerData.Rows)
            {
                address = new StreetAddress(row[2].ToString());
                Addresses.Add(address); // Address
            }
        }
        /// <summary>
        /// Orders the all street address by street name part on the address
        /// </summary>
        /// <returns>Ordered list of street addresses</returns>
        public List<string> GetOrderedAddresses()
        {
            List<string> OrderedAddresses = new List<string>();
            foreach (var line in Addresses
                         .Select(group => new
                         {
                             Number = group.HouseNumber,
                             Name = group.StreetName
                         })
                         .OrderBy(y => y.Name))
            {
                OrderedAddresses.Add(string.Format(line.Number + " " + line.Name));                   
            }
            return OrderedAddresses;
        }
    }
    
}
