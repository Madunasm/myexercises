﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.Text.RegularExpressions;


namespace Outsurance.Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Get and Read data file
                Console.WriteLine("Reading the input file");
                DataFileManagement dfmCustomer = new DataFileManagement();
                dfmCustomer.DataFilePath = @"C:\OutSurance\data.csv";

                DataTable customerData = dfmCustomer.GetDataFromCsvFile();


                //Get Ordered Name Frequency
                Console.WriteLine("Writing the Ordered Name Frequency file");
                Customers customers = new Customers(customerData);
                List<string> nameFrequencies = new List<string>();
                nameFrequencies = customers.GetOrderedNameFrequency();

                DataFileManagement dfmOrderedNameFrequency = new DataFileManagement();
                dfmOrderedNameFrequency.DataFilePath = @"C:\OutSurance\OrderedNameFrequency.csv";
                dfmOrderedNameFrequency.WriteDataToFile(nameFrequencies);

                //Get Ordered Addresses
                Console.WriteLine("Writing the Ordered Addresses file");
                StreetAddresses addresses = new StreetAddresses(customerData);
                List<string> orderAddresses = new List<string>();
                orderAddresses = addresses.GetOrderedAddresses();

                DataFileManagement dfmOrderedAddresses = new DataFileManagement();
                dfmOrderedAddresses.DataFilePath = @"C:\OutSurance\OrderedAddresses.csv";
                dfmOrderedAddresses.WriteDataToFile(orderAddresses);

                Console.WriteLine("Program run successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("An error occurred: {0}", ex.Message));
            }
            Console.WriteLine("Press enter key to close program..");
            Console.ReadLine();

        }      
       
    }
}

